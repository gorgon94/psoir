﻿using System;
using System.Threading;
using Amazon.DynamoDBv2;
using Amazon.S3;
using Amazon.SQS;
using AwsConsoleApp1.Services;
using AWSWorker.AWSServices;

namespace AWSWorker
{
    class Program
    {
        private static MessageHandler messageHandler;
        private static SQSService sqsService;
        private static S3Service s3Service;
        private static DynamoDBService dynamoDBService;

        private static string queueUrl;

        public static void Main(string[] args)
        {
            Initialize();
            EnableApi();
        }

        private static void Initialize()
        {
            queueUrl = AppDataFileService.FifoQueueUrl;
            string AWSAccessKey = AppDataFileService.AWSAccessKey;
            string AWSSecretKey = AppDataFileService.AWSSecretKey;

            AmazonSQSClient amazonSQSClient = new AmazonSQSClient(AWSAccessKey, AWSSecretKey);
            AmazonS3Client amazonS3Client = new AmazonS3Client(AWSAccessKey, AWSSecretKey);
            AmazonDynamoDBClient amazonDynamoDBClient = new AmazonDynamoDBClient(AWSAccessKey, AWSSecretKey);

            sqsService = new SQSService(amazonSQSClient, queueUrl);
            s3Service = new S3Service(amazonS3Client);
            dynamoDBService = new DynamoDBService(amazonDynamoDBClient);

            messageHandler = new MessageHandler(s3Service, sqsService, dynamoDBService);
        }

        private static void EnableApi()
        {
            Console.WriteLine("------------------------------AWS Listener started-----------------------------");
            while (true)
            {
                var messagesList = sqsService.CheckForMessagesList();

                foreach (var message in messagesList.Result)
                {
                    Thread thread = new Thread(() => messageHandler.Handle(message));
                    thread.Start();
                    Thread.Sleep(50);
                }
                Console.WriteLine("-----No messages-----");
                Thread.Sleep(20);
            }
        }
    }
}