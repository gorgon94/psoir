﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Threading.Tasks;
using AWSWorker.AWSServices;

namespace AWSWorker.Services
{
    class ImageService
    {
        private S3Service s3Service;

        public ImageService(S3Service s3Service)
        {
            this.s3Service = s3Service;
        }

        public void RotateImage90(string filePath)
        {
            RotateImage(RotateFlipType.Rotate90FlipNone, filePath);
        }

        public void RotateImage180(string filePath)
        {
            RotateImage(RotateFlipType.Rotate180FlipNone, filePath);
        }

        public void RotateImage270(string filePath)
        {
            RotateImage(RotateFlipType.Rotate270FlipNone, filePath);
        }

        public async void SetGrayscaleImage(string filePath)
        {
            try
            {
                Console.WriteLine($"changing to grayscale: {filePath} in progress");
                Image image = await GetImageFromBucket(filePath);
                ImageFormat imageFormat = image.RawFormat;
                Image grayImage = ConvertToGrayscale(new Bitmap(image));

                string response = await PutImageToBucket(grayImage, imageFormat, filePath);
                Console.WriteLine($"changing to grayscale and putting: {filePath} {response}");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private Bitmap ConvertToGrayscale(Bitmap source)
        {
            try
            { 
                Bitmap bm = new Bitmap(source.Width, source.Height);
                for (int y = 0; y < bm.Height; y++)
                {
                    for (int x = 0; x < bm.Width; x++)
                    {
                        Color c = source.GetPixel(x, y);
                        int average = (Convert.ToInt32(c.R) + Convert.ToInt32(c.G) + Convert.ToInt32(c.B)) / 3;
                        //int average = (int)(c.R * 0.3 + c.G * 0.59 + c.B * 0.11); 
                        bm.SetPixel(x, y, Color.FromArgb(average, average, average));
                    }
                }
                return bm;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private async void RotateImage(RotateFlipType rotateFlipType, string filePath)
        {
            try
            { 
                Console.WriteLine($"rotating {rotateFlipType}: {filePath} in progress");
                Image image = await GetImageFromBucket(filePath);
                ImageFormat imageFormat = image.RawFormat;
                image.RotateFlip(rotateFlipType);
                string response = await PutImageToBucket(image, imageFormat, filePath);
                Console.WriteLine($"rotating and putting {rotateFlipType}: {filePath} {response}");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private async Task<Image> GetImageFromBucket(string filePath)
        {
            try
            { 
                Stream getObjectStream = await s3Service.GetObjectStream(filePath);
                return Image.FromStream(getObjectStream);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private async Task<string> PutImageToBucket(Image image, ImageFormat imageFormat, string filePath)
        {
            try
            { 
                var ms = new MemoryStream();
                image.Save(ms, imageFormat);
                return await s3Service.PutObjectStream(ms, filePath);
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
