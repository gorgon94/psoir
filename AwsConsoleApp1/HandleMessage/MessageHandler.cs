﻿using Amazon.SQS.Model;
using AwsConsoleApp1.Services;
using AWSWorker.AWSServices;
using AWSWorker.Services;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AWSWorker
{
    class MessageHandler
    {
        private S3Service s3Service;
        private SQSService sqsService;
        private DynamoDBService dynamoDBService;
        private ImageService imageService;

        public MessageHandler(S3Service s3Service, SQSService sqsService, DynamoDBService dynamoDBService)
        {
            this.s3Service = s3Service;
            this.sqsService = sqsService;
            this.dynamoDBService = dynamoDBService;
            imageService = new ImageService(s3Service);
        }

        public void Handle(Message message)
        {
            Console.WriteLine("\n--------------------------------------------------------------");
            Console.WriteLine($"{message.MessageId} message recived");
            try
            {
                Dictionary<string, MessageAttributeValue> messageAttributes = message.MessageAttributes;
                string operation = messageAttributes["operation"].StringValue;
                string filePath = messageAttributes["key"].StringValue;
                switch (operation)
                {
                    case "Rotate 90": imageService.RotateImage90(filePath); break;
                    case "Rotate 180": imageService.RotateImage180(filePath); break;
                    case "Rotate 270": imageService.RotateImage270(filePath); break;
                    case "Grayscale": imageService.SetGrayscaleImage(filePath); break;
                    default: Console.WriteLine("No such operation available"); break;
                }
                dynamoDBService.PutLogInDynamoDb(operation, filePath);
            }
            catch(Exception e)
            {
                Console.WriteLine("--------------------------------------------------------------");
                Console.WriteLine("Wrong message\n"+e);
                Console.WriteLine("--------------------------------------------------------------");
            }
            sqsService.DeleteMessageFromQuery(message);
        }
    }
}
