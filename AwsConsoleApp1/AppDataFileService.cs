﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;

namespace AwsConsoleApp1.Services
{
    class AppDataFileService
    {
        private static Dictionary<string, string> ParseJsonFile()
        {
            return JsonConvert.DeserializeObject<Dictionary<string, string>>(File.ReadAllText("appData.json"));
        }

        public static string AWSAccessKey
        {
            get
            {
                var appData = ParseJsonFile();
                return appData["awsAccessKey"];
            }
        }

        public static string AWSSecretKey
        {
            get
            {
                var appData = ParseJsonFile();
                return appData["awsSecretKey"];
            }
        }

        public static string FifoQueueUrl
        {
            get
            {
                var appData = ParseJsonFile();
                return appData["fifoQueueUrl"];
            }
        }

        public static string BucketName
        {
            get
            {
                var appData = ParseJsonFile();
                return appData["bucketName"];
            }
        }

        public static string DynamoDBTableName
        {
            get
            {
                var appData = ParseJsonFile();
                return appData["dynamoDBTableName"];
            }
        }
    }
}
