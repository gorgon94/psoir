﻿using Amazon.SQS;
using Amazon.SQS.Model;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AwsConsoleApp1.Services
{
    public class SQSService
    {
        private AmazonSQSClient amazonSQSClient;
        private ReceiveMessageRequest receiveMessageRequest;
        private string queueUrl;

        public SQSService(AmazonSQSClient amazonSQSClient, string queueUrl)
        {
            this.queueUrl = queueUrl;
            this.amazonSQSClient = amazonSQSClient;

            receiveMessageRequest = new ReceiveMessageRequest
            {
                AttributeNames = new List<string>() { "All" },
                MaxNumberOfMessages = 5,
                QueueUrl = this.queueUrl,
                VisibilityTimeout = (int)TimeSpan.FromMinutes(10).TotalSeconds,
                WaitTimeSeconds = (int)TimeSpan.FromSeconds(5).TotalSeconds,
                MessageAttributeNames = new List<string>() { "operation", "key" },
            };

        }

        public async Task<List<Message>> CheckForMessagesList()
        {
            var response = await amazonSQSClient.ReceiveMessageAsync(receiveMessageRequest);
            return response.Messages;
        }

        public async void DeleteMessageFromQuery(Message message)
        {
            DeleteMessageRequest deleteMessageRequest = new DeleteMessageRequest
            {
                QueueUrl = queueUrl,
                ReceiptHandle = message.ReceiptHandle
            };

            DeleteMessageResponse deleteResponse = await amazonSQSClient.DeleteMessageAsync(deleteMessageRequest);
            Console.WriteLine($"{message.MessageId} deleteResponse:  {deleteResponse.HttpStatusCode}");
        }

    }
}
