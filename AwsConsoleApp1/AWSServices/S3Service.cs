﻿using Amazon.S3;
using Amazon.S3.Model;
using AwsConsoleApp1.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace AWSWorker.AWSServices
{
    class S3Service
    {
        private AmazonS3Client amazonS3Client;
        private string bucketName;

        public S3Service(AmazonS3Client amazonS3Client)
        {
            this.amazonS3Client = amazonS3Client;
            bucketName = AppDataFileService.BucketName;
        }

        public async Task<Stream> GetObjectStream(string filePath)
        {
            GetObjectRequest getObjectRequest = new GetObjectRequest
            {
                BucketName = this.bucketName,
                Key = filePath,
            };
            GetObjectResponse response = await amazonS3Client.GetObjectAsync(getObjectRequest);
            return response.ResponseStream;
        }

        public async Task<string> PutObjectStream(Stream inputStream, string filePath)
        {
            DeleteObjectRequest deleteObjectRequest = new DeleteObjectRequest
            {
                BucketName = bucketName,
                Key = filePath,
            };
            DeleteObjectResponse deleteResponse = amazonS3Client.DeleteObject(deleteObjectRequest);
            Console.WriteLine($"Object {filePath} deleted OK");

            //filePath = new Random(DateTime.Now.Millisecond).Next() % 1000 + " " + filePath.Substring(3);
            Thread.Sleep(10);
            PutObjectRequest putObjectRequest = new PutObjectRequest
            {
                BucketName = bucketName,
                Key = filePath,
                CannedACL = S3CannedACL.PublicRead,
                Grants = new List<S3Grant>()
                {
                    new S3Grant()
                    {
                        Grantee = new S3Grantee()
                        {
                            CanonicalUser = "Everyone",
                            DisplayName = "Everyone"
                        },
                        Permission = new S3Permission("open/download")
                    }
                },              
            };
            putObjectRequest.Headers.CacheControl = "max-age=0";
            putObjectRequest.InputStream = inputStream;
            PutObjectResponse response = await amazonS3Client.PutObjectAsync(putObjectRequest);
            return response.HttpStatusCode.ToString();
        }
    }
}
