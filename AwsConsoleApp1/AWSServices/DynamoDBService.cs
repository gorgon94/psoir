﻿using System;
using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DocumentModel;
using AwsConsoleApp1.Services;

namespace AWSWorker.AWSServices
{
    class DynamoDBService
    {
        private AmazonDynamoDBClient amazonDynamoDBClient;
        private string dynamoDBTableName;

        public DynamoDBService(AmazonDynamoDBClient amazonDynamoDBClient)
        {
            this.amazonDynamoDBClient = amazonDynamoDBClient;
            dynamoDBTableName = AppDataFileService.DynamoDBTableName;
        }

        public async void PutLogInDynamoDb(string operation, string filePath)
        {
            var table = Table.LoadTable(amazonDynamoDBClient, dynamoDBTableName);
            var item = new Document
            {
                ["date"] = DateTime.Now.ToString("yyyy-MM-dd HH:mm"),
                ["operation"] = operation,
                ["filePath"] = filePath,
            };

            await table.PutItemAsync(item);
            Console.WriteLine($"Operation {operation} on file {filePath} logged in {dynamoDBTableName}");
        }
    }
}
